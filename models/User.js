'use strict';

const Model = require('./util/Model');

const bcrypt = require('bcrypt');

const jwt = require('../lib/jwt');
const mailgun = require('../lib/mailgun');

class User extends Model {
    constructor() {
        super('User');
    }

    async create(email, password) {
        const usersWithEmail = await super.query(function (query) {
            query.filter('email', '=', email);
        });

        if (usersWithEmail.length !== 0) {
            const validated = usersWithEmail.find(function (user) {
                return user.validated;
            });

            if (validated) {
                throw new KoaError('This email is already taken.', 400);
            }

            let user;
            for (let i = 0; i < usersWithEmail.length; i++) {
                const u = usersWithEmail[i];
                if (await bcrypt.compare(password, u.password)) {
                    user = u;
                    break;
                }
            }

            if (user) {
                return {id: user.id, email, validated: false};
            }
        }

        const hash = await bcrypt.hash(password, 10);
        const id = await super.insert({email, password: hash, validated: false, messages: 0, messages_month: 0});

        const emailJWT = jwt.createEmailJWT(id, email);
        const verifyLink = `http://${_config.domain}/verify/${emailJWT}`;
        const data = {
            to: email,
            subject: 'WAAS Sign Up Email Verification',
            html: `<html><head></head><body><a href="${verifyLink}">Click here to verify your WAAS account.</a> This link expires in 24 hours.</body></html>`
        };

        mailgun.send(data);

        return {id, email, validated: false};
    }

    async validate(email, password) {
        const usersWithEmail = await super.query(function (query) {
            query.filter('email', '=', email);
        });

        if (usersWithEmail.length === 0) {
            throw new KoaError('Invalid email or password.', 400);
        }

        let user;
        for (let i = 0; i < usersWithEmail.length; i++) {
            const u = usersWithEmail[i];
            if (await bcrypt.compare(password, u.password)) {
                user = u;
                break;
            }
        }

        if (user === undefined) {
            throw new KoaError('Invalid email or password.', 400);
        }

        const jwtData = {id: user.id, email};

        if (user.validated === false) {
            jwtData.validated = false;
        }

        return jwtData;
    }

    async validateEmail(user) {
        const usersWithEmail = await super.query(function (query) {
            query.filter('email', '=', user.email);
        });

        if (usersWithEmail.length === 0) {
            throw new KoaError('Email verification link expired or invalid.', 400);
        }

        const validatedUser = usersWithEmail.find(function (userToTest) {
            return userToTest.validated;
        });

        if (validatedUser) {
            if (validatedUser.id !== user.id) {
                throw new KoaError('User with that email already exists.', 400);
            } else {
                throw new KoaError('You have already validated your email.', 400);
            }
        }

        await this.transaction(async function (transaction) {
            const userData = usersWithEmail.find(function (userToTest) {
                return user.id === userToTest.id;
            });

            userData.validated = true;
            delete userData.id;

            await transaction.save(user.id, userData);

            const usersToRemove = usersWithEmail.filter(function (userToTest) {
                return userData !== userToTest;
            }).map(function (userToTest) {
                return userToTest.id;
            });

            await transaction.delete(usersToRemove);
        });
    }
}

module.exports = new User();