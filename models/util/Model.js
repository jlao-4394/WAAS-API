'use strict';
const config = _config.spanner;

const Datastore = require('@google-cloud/datastore');
const datastore = Datastore({
    projectId: config.projectId
});

class DatastoreInterfacer {
    constructor(kind, datastoreInterface) {
        this.kind = kind;
        this.interface = datastoreInterface;
    }

    async insert(data) {
        const key = datastore.key(this.kind);
        await this.interface.insert({key, data});
        return key.id;
    }

    async get(rawKey) {
        return await this.interface.get(this.wrapKey(rawKey));
    }

    async query(queryModifier) {
        const query = this.interface.createQuery(this.kind);

        queryModifier(query);

        const entities = (await query.run())[0];

        entities.forEach(function (entity) {
            entity.id = entity[datastore.KEY].id;
        });

        return entities;
    }

    async update(rawKey, data) {
        const key = this.wrapKey(rawKey);
        return await this.interface.update({key, data});
    }

    async save(rawKey, data) {
        const key = this.wrapKey(rawKey);
        return await this.interface.save({key, data});
    }

    async delete(rawKey) {
        return await this.interface.delete(this.wrapKey(rawKey));
    }

    wrapKey(rawKey) {
        if (Array.isArray(rawKey)) {
            return rawKey.map((rawKey) => {
                return datastore.key([this.kind, datastore.int(rawKey)]);
            });
        } else {
            return datastore.key([this.kind, datastore.int(rawKey)]);
        }
    }
}

class Model extends DatastoreInterfacer {
    constructor(kind) {
        super(kind, datastore);
    }

    async transaction(transactionModifier, retries) {
        const transaction = new Transaction(this.kind);
        await transaction.run();
        try {
            await transactionModifier(transaction);
            await transaction.commit();
        } catch (e) {
            await transaction.rollback();

            if (retries === 0) {
                throw e;
            }

            await this.transaction(transactionModifier, retries === undefined ? retries - 1 : 5)
        }
    }
}

class Transaction extends DatastoreInterfacer {
    constructor(kind) {
        super(kind, datastore.transaction());
    }

    async commit() {
        return await this.interface.commit();
    }

    async rollback() {
        return await this.interface.rollback();
    }

    async run() {
        return await this.interface.run();
    }
}

module.exports = Model;