global._config = require(`./config/${process.env.NODE_ENV || 'development'}.json`);
class KoaError extends Error {
    constructor(message, status) {
        super(message);
        this.status = status;
    }
}
global.KoaError = KoaError;

const Koa = require('koa');
const app = new Koa();
const routes = require('./routes');

const compress = require('koa-compress')();
const bodyParser = require('koa-json-body')();

app
    .use(async function (ctx, next) {
        try {
            await next();
        } catch (err) {
            if (err instanceof KoaError) {
                ctx.response.status = err.status;
                ctx.response.message = err.message;
            } else {
                console.error(err);
                ctx.response.status = 500;
            }
        }
    })
    .use(compress)
    .use(bodyParser)
    .use(routes)
    .listen(8080);