'use strict';
const secret = _config.jwt.secret;

const jsonwebtoken = require('jsonwebtoken');

module.exports = {
    sign(payload, options) {
        return jsonwebtoken.sign(payload, secret, options);
    },
    verify(token, options) {
        try {
            return jsonwebtoken.verify(token, secret, options);
        } catch (e) {
            return undefined;
        }
    },
    createUserJWT(id, email, validated) {
        return this.sign({id, email, validated}, {
            expiresIn: '15d'
        });
    },
    createEmailJWT(id, email) {
        return this.sign({id, email}, {
            expiresIn: '1d'
        });
    }
};