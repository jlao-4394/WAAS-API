"use strict";

const mailgun = require('mailgun-js')(_config.mailgun.options);

module.exports = {
    async send(data) {
        const mergedData = Object.assign(data, _config.mailgun.send);
        return await mailgun.messages().send(mergedData);
    }
};