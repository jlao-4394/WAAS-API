'use strict';

const Models = require('../models');

const jwt = require('../lib/jwt');

module.exports = function (router) {
    router.post('/signup', async function (ctx, next) {
        const user = await Models.User.create(ctx.request.body.email, ctx.request.body.password);
        ctx.response.body = jwt.createUserJWT(user.id, user.email, user.validated);
    });
};