'use strict';

const Models = require('../models');

const jwt = require('../lib/jwt');

module.exports = function (router) {
    router.get('/verify/:jwt', async function (ctx, next) {
        const user = jwt.verify(ctx.params.jwt);

        if (user === undefined) {
            throw new KoaError('Email verification link expired or invalid.', 400);
        }

        await Models.User.validateEmail(user);
        ctx.response.body = jwt.createUserJWT(user.id, user.email);
    });
};